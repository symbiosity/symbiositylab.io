---
layout: blog
title: Formations | Symbiosity
---

# Fire Détection Management System

{: style="text-align:center"}
![Android View](/assets/img/portfolio/monitoring1.png)

Symbiosity is a Startup Company that will provide fire detection monitoring and safety solution to commercial/industrial properties through an Emergency Control Centre to monitor and manage their fire risk and fire detection systems and then liaison real time fire detection and emergencies to the nearest local fire department to respond to the location. 

## Install
Symbiosity will install, maintain and monitor the fire detection management system on behalf of the customers, while giving them innovative technology back with specialist who have the expertise to monitor and analyze the potential threats. 

## Market Size

A recent report published by _Markets and Markets_, a global market research and consulting company based in the USA, has forecast that the fire protection systems market will grow from US$34.74 billion in 2013 to US$66.56 billion in 2018, representing a compound annual growth rate (CAGR) of 13.9 percent during the forecast period. 

## How it works

### Remote Monitoring

Symbiosity will offer real time fire detection data via a `web based application`, while reducing cost. The systems will produce an early “pre-alarm” warning, based on the level of detection, which allows the responsible person to investigate potential alarms before the system activates its fire alarm. The web based application will facilitate remote monitoring by trained staff, generate real time reports, map location with clearly defined floor plan, displaying routes to the distress location, as well as nearby standpipe/hydrants, building wet/dry risers, stairway and emergency shutoff locations and systems throughout the building. 

#### Initial Offerings
Symbiosity will offer addon services such as: 

* Consulting 
* Building evacuation planning 
* Fire system layout, suppression system
* Sprinkler and fire extinguisher layout. 

Customers pay a tiered subscription fee monthly based on the number of :
<ol type="A">
 <li>detectors to be monitored</li>
 <li>building square footage</li>
</ol>


#### Keep in touch


Feel free to ask <a href="mailto:{{ site.data.members.moses.email }}" title="{{ site.data.members.moses.email }}">
  <i class="fa fa-envelope-o fa-x" data-wow-delay=".2s"></i> {{ site.data.members.moses.email }}
</a> for information solution.
 <!--mailchimp popup script--><script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"7de0c41b59cc6d422a4eb51c3","lid":"1d766bf5f3"}) })</script>
