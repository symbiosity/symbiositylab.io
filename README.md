# --> !!!FORKED!!! <--

## ChillCoding.com

Serve the site:

```
bin/serve
```

Or you can run the latest Docker image:

```
docker run -p 4000:4000 registry.gitlab.com/chillcoding-at-the-beach/chillcoding-at-the-beach.gitlab.io
```

SYMBIOSITY, INC - Remote Fire Detection and Monitoring

Symbiosity is at your service, in an age of sky-scraper buildings and the never-ceasing demands for a 
greater degree of FireFighting technology.

wireless detectors that should detect smoke, fire, flame, heat all in one and also has the capabilities to 
detect temperature increase and infra red technology to see heat source.

fire suppression detector that should have 360 degrees rotation equipt with infra red technology to 
located the heat/fire/flame/ source location and penetrate that hazard location with suppression media.

Symbiosity will fire detection monitoring and safety solution to commercial/industrial properties through an Emergency Control Centre to monitor and manage their fire risk and fire detection systems and then liaison real time fire detection and emergencies to the nearest local fire department to respond to the location. 
Symbiosity will install, maintain and monitor the fire detection management system on behalf of the customers, while giving them innovative technology back with specialist who have the expertise to monitor and analyze the potential threats. 
A recent report published by Markets and Markets, a global market research and consulting company based in the USA, has forecast that the fire protection systems market will grow from US$34.74 billion in 2013 to US$66.56 billion in 2018, representing a compound annual growth rate (CAGR) of 13.9 percent during the forecast period. 
Symbiosity will offer real time fire detection data via a web based application, while reducing cost. The systems will produce an early “pre-alarm” warning, based on the level of detection, which allows the responsible person to investigate potential alarms before the system activates its fire alarm. The web based application will facilitate remote monitoring by trained staff, generate real time reports, map location with clearly defined floor plan, displaying routes to the distress location, as well as nearby standpipe/hydrants, building wet/dry risers, stairway and emergency shutoff locations and systems throughout the building. 
Symbiosity will offer addon services such as consulting, building evacuation planning, fire system layout, suppression system, sprinkler and fire extinguisher layout. Customers pay a tiered subscription fee monthly based on the number of detectors to be monitored and building square footage and get their building equipped and setup with the *hardware and *software at a low cost, saving them capital. 
Symbiosity Fire Detection Management System is currently at the conceptual stage, researching the hardware and software solutions more so the ONLXYWorks software for integration with Symbiosity solution offering.
