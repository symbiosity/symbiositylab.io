---
title: "Fire Extinguisher Training"
categories: fire and safety training
author: moses
last_update: 2018-06-23
---

<div class="text-center lead" markdown="1">
  ![Fire Extinguisher Training](/assets/img/post/extinguisher-training.png)
</div>

<b style='color:black'>Fire Extinguisher Training</b>,

Duration: This course lasts up to 2 hours and is normally conducted in 2 sessions - theory & practical.

Prices start from $2500 per staff depending on group sizes.

Group size of a maximum 30 staffs per session.

This course is designed for all staff, particularly those who have specific 
fire & safety responsibilities (such as Fire Marshals/Fire Wardens/Safety Marshals, in addition to their specific fire marshall training). 
The course will also greatly benefit anyone who may need to operate fire extinguishers and other fire fighting equipment, 
the incorrect operation of which is ineffective and can in some cases be dangerous.

Our fire extinguisher training course enable staffs to manage fire situations in the workplace. 
Our customised training provides all the knowledge and practical experience needed to extinguish 
a small fire at its incipient stage within the work environment.

It’s essential that everyone knows how to use a portable fire extinguisher safely and effectively. 
Our trainers have years of experience working in the fire service and 
qualified to impart fire extinguisher theory and practical sessions.

<b style='color:#00bfff'>*THIS COURSE INVOLVES PRACTICAL FIRE EXTINGUISHER TRAINING*</b>,


<!--more-->

## Additional Information on the Fire Extinguisher Training

All our fire extinguisher training sessions have a theory session and a “live fire” hands-on pratical session - our fire 
safety training expert will bring a range of portable extinguishers to use or you can provide your own 
extinguishers for the practical session. 

A ‘live fire’ practical will help create a more realistic session and will improve everyone’s learning experience.
Training should take place during normal working hours and be repeated periodically.
If there are any other elements of training that you feel should be included in 
the training course, then please let us know and we’ll incorporate it into your sessions.

Once complete, staff members who have attended this course will be able to apply good fire 
safety practice at work and correctly operate fire fighting equipment. All staff will have had 
practical experience using a fire extinguisher, and receive a competence certificate.


##Don’t forget to update emergency training. 

OSHA also provides guidelines on
when to provide emergency training. Clearly, it’s not enough simply to train once
and forget about it. OSHA encourages emergency training:
 Immediately after developing an EAP
 After revisions to the EAP
 For all new employees
 For employees with new responsibilities or assignments
 When new equipment, materials, or processes are introduced
 When exercises and drills show unsatisfactory performance
 In any event, at least annually
