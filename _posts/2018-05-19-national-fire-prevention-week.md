---
title: "National Fire Prevention Week"
categories: Fire Prevention
author: moses 
---

<div class="text-center lead" markdown="1">
  ![Prevention Week](/assets/img/post/PreventionWeek.png)
</div>

National Fire Prevention Week has been a yearly campaign since 1922. It was established to commemorate the Great Chicago Fire on October 8-9, 1871. This infamous tragedy claimed 250 lives, destroyed more than 17,400 structures and left 100,000 people homeless. Fire prevention week is a day to remember this event and also what kind of effect a fire could potentially have on our lives.
<!--more-->

## National Fire Prevention Week

No matter what kind of business you own or manage, October is also the perfect time to remember how devastating fire can be, and how important it is to be ready for an emergency. A fire can cause serious damage to your company’s facility and equipment, not to mention employees! This is as good a month as any to make sure you have these five ‘checks’ taken care of within the boundaries of your office and/or facilities.

1. Are your fire hazard risks assessed properly? Some businesses have greater fire risks than others, but there are very few businesses that have none. They all need to be properly assessed so the proper prevention can be implemented accordingly. Some local governments offer fire marshal visits, or workplace fire risk assessment guidance from your building’s property manager.  A commercial fire safety firm can also help you mitigate problems.

2. Do you have emergency plans in place? Do you have an evacuation plan and do your employees know what to do in case of a fire? Do they have fire training, so that they know how to use fire extinguishers, and when to use them?

3. Do you have the right fire protection equipment installed? Your fire safety equipment needs likely include sprinkler systems, but you might need more to be up to code or compliant with regulations. Industries dealing with machinery that overheats or flammable substances might need a suppression system tailored to your company.

4. Do you have scheduled routine equipment inspections?  Even if you have the right sprinkler systems, fire extinguishers, and suppression systems, they also need routine inspections (at least annually) to keep everything in working order. Know what tests you can do yourself, and which require professional visits from fire alarm technicians.


## Live Fire Detection Monitoring

Even though Fire Prevention Week is over, it’s not too late to think about your fire prevention and safety. Perhaps now is the time to take action. [Contact Us](http://www.symbiosity.co/?#contact) for more information on any of the fire steps detailed above, from alarm monitoring to employee training programs.
