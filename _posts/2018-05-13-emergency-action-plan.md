---
title: "What should an EAP contain"
categories: Fire and Safety
author: moses
---

<div class="text-center lead" markdown="1">
  ![Android Vue](/assets/img/post/eap.png)
</div>

## What should an Emergency Action Plan contain?

OSHA includes helpful guidelines for EAPs as an
appendix to its standard on Exit Routes, EAPs, and Fire Prevention Plans (29 CFR
1910, Subpart E).

In brief, the EAP should address any emergencies that might reasonably
be expected to happen in your workplace and include:
 Procedures for reporting the emergency
 Evacuation procedures—ideally, the EAP should include floor plans showing
exit routes and assembly points
 How to account for all employees who have evacuated
 Responsibilities of any employees who are designated to stay behind and
ensure safe shutdown of operations
 Responsibilities of any employees who may be designated to perform rescue
or medical duties

<!--more-->

## Who needs to have an Emergency Action Plan? 
There are two different, but
equally correct, answers to this question. The first answer is that OSHA requires an
EAP only for employers that are covered by certain standards, such as “Fixed Extinguishing
Systems” and “Process Safety Management of Highly Hazardous Chemicals”
(other standards also require EAPs). The second answer is that every
company really should have an EAP. Not only does OSHA explicitly recommend it,
but it simply makes sense to have a plan for a safe, orderly response to emergencies
such as fires, weather events, and releases of hazardous substances. And note
that even relatively minor incidents, such as small fires or spills, constitute an
“emergency” if they trigger an alarm and require employees to stop what they’re
doing and evacuate their work areas.