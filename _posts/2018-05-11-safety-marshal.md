---
title: "Safety Marshal"
categories: Fire and Safety
author: moses
---

<div class="text-center lead" markdown="1">
  ![Android Kotlin](/assets/img/post/safety-marshal.png)
</div>

## Overview

Our half-day course for Safety Marshals (Fire Marshals/Fire Wardens/Safety Wardens) ensures that participants
achieve a thorough awareness and understanding of the
threat posed by fire. All delegates receive practical hands on
training in fire extinguishers as well as interactive training in
risk assessment, mitigation and a copy of our ‘Fire Warden
Handbook’ (RRP £20.00).
This course is available as an open course, or as a bespoke
course delivered at clients’ venues at a time suited to them.
<!--more-->

## Who will benefit?

This course will be of benefit to all staff, particularly those
designated as fire wardens or marshals. It is suitable for
employees working in industry, commerce or the public sector.

## Learning outcomes

You will develop knowledge and skills in how to:
• identify fire hazards and risks – Unit FS1.1*
• report fire hazards and risks – Unit FS1.2*
*Units refer to the National Occupational Standards (NOS) in
Fire Safety 2010.

## Programme

The course content includes:
• fire legislation, facts and figures
• combustion principles and how fire spreads
• fire case study
• how to spot common hazards
• actions in the event of fire and evacuation principles
• fire wardens’ responsibilities
• routine fire safety checks
• fire extinguisher theory and practical
