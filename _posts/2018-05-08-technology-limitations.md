---
title: "Technology Limitations"
categories: fire and safety technology
author: moses
---

<div class="text-center lead" markdown="1">
  ![Technology Limitations](/assets/img/post/tech-limitation.png)
</div>

Technology Limitations
There are really two main types of legacy fire alarm systems to consider, 
each with its own inherent problems: conventional fire alarm systems and addressable or intelligent systems.

For most applications, conventional systems should be upgraded to state-of-the-art addressable or 
intelligent fire alarm systems. That's because older conventional fire alarm systems are time-consuming 
and expensive to troubleshoot. Because conventional systems are limited in the amount of information they 
can provide the technician, troubleshooting often becomes tedious and labor intensive. Additionally, 
wire-to-wire shorts typically report as a false alarm event and can cause mission or business continuity losses. 
In most cases, large conventional fire alarm systems are obsolete and parts are difficult to obtain.

https://www.facilitiesnet.com/firesafety/article/Upgrading-Notification-Systems-To-Code-Can-Create-Technical-Challenges--13775