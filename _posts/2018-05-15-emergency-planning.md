---
title: "Emergency Planning"
categories: fire and safety
author: moses
---

<div class="text-center lead" markdown="1">
  ![Android Vue](/assets/img/post/emargency1.png)
</div>

Nobody expects an emergency or disaster – especially one that
affects them, their employees, and their business personally.
Yet the simple truth is that emergencies and disasters can strike
anyone, anytime, and anywhere. You and your employees could
be forced to evacuate your company when you least expect it.
<!--more-->

## Workplace Emergency Planning

A workplace emergency is an unforeseen situation that threatens
your employees, customers, or the public; disrupts or shuts
down your operations; or causes physical or environmental damage.
Emergencies may be natural or manmade and include the following:

## Protect
The best way to protect yourself, your workers, and your
business is to expect the unexpected and develop a well-thoughtout
emergency action plan to guide you when immediate action is
necessary.


## Prepare
The best way is to prepare to respond to an emergency before
it happens. Few people can think clearly and logically in a crisis,
so it is important to do so in advance, when you have time to be
thorough.
