---
title: "Conducting a Fire Risk Assessment."
categories: fire safety
author: moses
---

<div class="text-center lead" markdown="1">
  ![Conducting a Fire Risk Assessment](/assets/img/post/risk-assessment.png)
</div>

Conducting a Fire Risk Assessment – 5 Steps

fire risk assessmentIf you are unsure as to the best approach to take when conducting the fire risk assessment at your facilities, there are government recommendations in place to assist you, separated into 5 steps.

The process should be applied to each area of your working environment (office, kitchens, bathrooms etc.), making the process more straightforward by compartmentalising it.

<!--more-->


#1 Identify the Fire Hazards

Beginning with your first area, carefully assess the space and make a note of any fire risks you come across. Bear the fire triangle in mind, as this will help you to evaluate the severity of any hazards. The triangle is an easy way to recall the 3 elements that cause a fire: a source of ignition, a source of oxygen, and a source of fuel.

#2 Identify People at Risk

As an employer or manager, you should have a pretty decent idea as to the size and makeup of your staff. However, those at risk from a fire in your facilities also include customers and other visitors, and particular attention should be paid to creating an evacuation procedure for those with physical disabilities or sensory impairments.

The elderly and young children are always considered to be among those most at risk.

#3 Evaluate, Remove and Reduce Risks

Based on what was discovered in steps 1 and 2, you must now take action towards the reduction and removal of any risks identified.

This might include assessing the status of fire safety equipment, such as fire extinguishers, smoke detectors and alarms, emergency lighting and exit signs. Passive fire resistance elements should also be considered, such as fire doors with an intumescent seal to help stop the spread of fire.

With any equipment that’s installed, a system must be implemented to schedule in regular testing and assessment, with maintenance carried out immediately if required.

#4 Record, Plan, Inform, Instruct and Train

Simply identifying hazards is not sufficient for completing the fire risk assessment. Firstly, anything discovered of note should be recorded accurately, preferably in separate logs for each area of your facilities.

Write down all hazards you encounter and the steps taken towards rectifying them, all dated for future reference.

This information can then help you to determine an evacuation plan to be implemented in the event of a fire. Your staff should be trained fully in every aspect of the plan, and regular drills should be carried out to practice.

#5 Review

Due to the changing and evolving nature of any facilities environment, your fire risk assessment should be reviewed and updated on a regular basis. While periodically scheduled reviews should be arranged, there should also be a new assessment performed when significant changes to the facilities occur (including structural alterations, noticeable increase in staff numbers etc.)

Checklist

The key to a successful facilities risk assessment is to keep it simple. Here’s a useful checklist we’ve put together to help you comply with the Regulatory Reform while upholding optimum health and safety measures in your working environment.

#1 What are the Fire Hazards within your Environment?
◦In respect of the fire triangle, this should include anything that is a source of ignition (i.e. cooking equipment, electrical components).
◦Similarly, anything that is a source of fuel (i.e. flammable solvents, cleaning materials, rubbish piles)

#2 Who is at Risk within your Environment?
◦This should consider all staff, customers and visitors.
◦Particular attention should be paid to vulnerable persons – the elderly, the very young, those with a disability, those working in high-risk areas.

#3 How will you keep People Safe within your Environment?
◦Keep all working environments within the facility clean and free of hazardous obstructions.
◦Storage of all equipment and materials used in the management of facilities (cleaning products, solvents etc.) should be carefully regulated.
◦Effective waste disposal procedures should be in place to prevent a build-up of rubbish bags and other waste.
◦Ensure that correct fire safety signage is present throughout the premises, indicating emergency exits and evacuation assembly points.
◦Have all electrical elements involved in the management of the facilities been PAT tested?
◦Are the fire extinguishers within the premises the ones most suited to the environment?

#4 Record, Plan and Train
◦Record all potential risks discovered during the assessment.
◦Take steps to remove or reduce each hazard wherever possible.
◦Keep all staff within the facilities informed and ensure that they are aware of, and trained in, the correct evacuation procedure and fire extinguisher use.
◦Plan regular fire drills to maintain good practice.
◦Draw up a detailed timeframe for carrying out all fire safety improvements required.
◦Keep all records and logs in a place that can be accessed by your staff.
◦Make sure that all fire exit, fire extinguisher types, assembly points and other evacuation information is clearly visible for any visitors to the premises.

#5 Review and Maintain your Plan
◦Any interior or exterior alterations to the structure of the premises would necessitate a revision of the fire risk assessment.
◦Any fires or close-calls should also prompt a review.
◦Should the working practices or the equipment used within the environment change, then the assessment will need to change accordingly.
◦A record should be kept of any fire drills carried out.
