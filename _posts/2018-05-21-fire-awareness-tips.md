---
title: Fire Awareness Tips
categories: Fire and Safety
author: moses
---

<div class="text-center lead" markdown="1">
  ![Android Requête HTTP](/assets/img/post/fire-awareness-tips.png)
</div>

## Fire Awareness Tips
Due to the serious nature of fire and its consequence it is vital that you are aware of what is best to do if you discover a fire. 
Every workplace is legally bound to provide its staff with details of fire exits, assembly points, and other guidelines about what 
to do in the event of a fire. It is the employees responsibility to familiarise themselves with this information.

<!--more-->

The following are a general list of advice that you should follow if you discover a fire:

You should try to extinguish a fire out only if you discover it in its early stages and 
you are completely confident that you can put it out. 
Be aware, however, that even a small fire can spread quickly and soon become out of control. 
You safety and your fellow workers safety comes first.

If you do decide to tackle the fire, make sure that if you can't control it that you can still escape and that your exit is not blocked. 
Also be aware that fire extinguishers are only designed to extinguish fires in their very early stages and that the extinguisher may not 
be adequate if the fire spreads and the room becomes filled with smoke or fumes.

If you believe that the fire cannot be easily dealt with you should immediately set off the nearest fire alarm by breaking the glass 
and then leave through the appropriate fire exit. The fire brigade should be contacted immediately.




