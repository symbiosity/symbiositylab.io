---
title: "Emergency Response Training"
categories: Fire and Safety Training
author: moses
---

<div class="text-center lead" markdown="1">
  ![Android Vue](/assets/img/post/emergency-training.png)
</div>

## What should emergency response training include?

Every employee needs
to know what he or she is expected to do when an emergency alarm sounds—and
furthermore, to do it quickly. For most employees, the proper response is simply to
evacuate the work area in a rapid but orderly manner, using proper exit routes, and
to assemble in a designated “safe area.” However, some employees—OSHA calls
them “evacuation wardens”—should be given the responsibility for making sure
that other employees leave the area properly and safely.

<!--more-->

## Safety Warden Training 

OSHA recommends one warden for every 20 employees and suggests that they should receive specialized
training in:
 Knowing the complete layout of the building or work area, including the various
exit routes
 Giving guidance and instruction to employees during evacuation
 Knowing how to assist employees, such as those with disabilities, who may
need assistance
 Checking all rooms and enclosed spaces to make sure that no one is left behind
 Accounting for all employees after evacuation is complete


